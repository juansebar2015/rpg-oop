//
//  Character.swift
//  RPG-OOP
//
//  Created by Juan Ramirez on 4/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Character {
    
    private var _hp: Int = 100
    private var _attackPower: Int = 100
    
    // Getters
    var attackPwr: Int {
        get{
            return _attackPower
        }
        
    }
    
    var hp: Int {
        get{
            return _hp
        }
    }
    
    var isAlive: Bool{
        get{
            if hp <= 0 {
                return false
            } else{
                return true
            }
        }
    }
    
    // Constructor
    init(startingHp: Int, attackPwr: Int){
        self._hp = startingHp
        self._attackPower = attackPwr
    }
    
    func attemptAttack(attackPwr: Int) -> Bool {
        self._hp -= attackPwr
        
        return true
    }
}