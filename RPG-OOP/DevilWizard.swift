//
//  DevilWizard.swift
//  RPG-OOP
//
//  Created by Juan Ramirez on 4/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class DevilWizard: Enemy {
    
    override var loot: [String] {
        return ["Magic Wand", "Dark Amulet", "Salted Pork"]
    }
    
    override var type: String {
        return "Devil Wizard"
    }
    
}