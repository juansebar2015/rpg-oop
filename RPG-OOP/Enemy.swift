//
//  Enemy.swift
//  RPG-OOP
//
//  Created by Juan Ramirez on 4/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Enemy: Character{
    
    var loot: [String] {
        return ["Rusty Dagger", "Cracked Buckler"]
    }
    
    var type: String {
        return "Grunt"
    }
    
    
    func dropLoot() -> String? {
        
        if !isAlive {
            let rand = Int(arc4random_uniform(UInt32(loot.count))) // Get random number from 0 to array size
            
            return loot[rand]
        }
        
        return nil
    }
    
}