//
//  Kimara.swift
//  RPG-OOP
//
//  Created by Juan Ramirez on 4/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Kimara: Enemy {
    
    let  IMMUNE_MAX = 15
    
    override var loot: [String] {
        return ["Tough Hide", "Kimara Venom", "Rare Trident"]
    }
    
    override var type: String {
        return "Kimara"
    }
    
    override func attemptAttack(attackPwr: Int) -> Bool {
        
        if attackPwr >= IMMUNE_MAX {
            return super.attemptAttack(attackPwr)
        } else {
            //Attack not successful
            return false
        }
    }
}