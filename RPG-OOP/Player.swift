//
//  Player.swift
//  RPG-OOP
//
//  Created by Juan Ramirez on 4/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

class Player: Character {
    
    private var _name = "Player"
    
    // Getter
    var name: String {
        get{
            return _name
        }
    }
    
    private var _inventory = [String]()
    
    // Getter
    var inventroy: [String] {
        get{
            return _inventory
        }
    }
    
    
    func addItemToInventory(item: String) {
        _inventory.append(item)
    }
    
    convenience init(name: String, hp: Int, attackPwr: Int) {
        
        self.init(startingHp: hp, attackPwr: attackPwr)
        
        _name = name
    }
    
}