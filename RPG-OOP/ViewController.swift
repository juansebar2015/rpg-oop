//
//  ViewController.swift
//  RPG-OOP
//
//  Created by Juan Ramirez on 4/21/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var printLabel: UILabel!
    @IBOutlet weak var playerHpLabel: UILabel!
    @IBOutlet weak var enemyHpLabel: UILabel!
    @IBOutlet weak var enemyImage: UIImageView!
    @IBOutlet weak var chestButton: UIButton!
    
    var player: Player!
    var enemy: Enemy!
    var chestMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        player = Player(name: "DirtyLaundry21", hp: 110, attackPwr: 20)
        playerHpLabel.text = "\(player.hp) HP"
        
        generateRandomEnemy()
        
    }
    
    func generateRandomEnemy() {
        
        let rand = Int(arc4random_uniform(UInt32(2)))
        print(rand)
        
        if rand == 0 {
            enemy = Kimara(startingHp: 50, attackPwr: 12)
        } else{
            enemy = DevilWizard(startingHp: 60, attackPwr: 15)
        }
        
        enemyHpLabel.text = "\(enemy.hp) HP"
        enemyHpLabel.textColor = UIColor.greenColor()
        
        enemyImage.hidden = false
    }

    @IBAction func onChestPress(sender: AnyObject) {
        
        chestButton.hidden = true
        printLabel.text = chestMessage
        
        //Time interval for a new enemy to appear
        NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "generateRandomEnemy", userInfo: nil, repeats: false)
        
    }

    @IBAction func attackPress(sender: AnyObject) {
        
        if enemy.attemptAttack(player.attackPwr) {

            printLabel.text = "Attacked \(enemy.type) for \(player.attackPwr) HP"
            enemyHpLabel.text = "\(enemy.hp) HP"
            
            if enemy.hp <= 20 {
                enemyHpLabel.textColor = UIColor.redColor()
            }
        } else {
            printLabel.text = "Attack was unsuccessful!"
        }
        
        if let loot = enemy.dropLoot() {
            player.addItemToInventory(loot)     // Add item
            chestMessage = "\(player.name) found \(loot)"
            chestButton.hidden = false
        }
        
        if !enemy.isAlive {
            enemyHpLabel.text = ""
            printLabel.text = "Killed \(enemy.type)"
            enemyImage.hidden = true
        }
    }

}

